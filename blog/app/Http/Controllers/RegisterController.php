<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    // public function index()
    // {
    //     return view('index');
    // }

    public function form()
    {
        return view('form');
    }
    public function done()
    {
        return view('done');
    }
    public function done_post(Request $request)
    {
       $fname = $request["fname"];
       $lname = $request["lname"];
        return view ("done", compact('fname', 'lname') );
        // return ("$fname $lname");
    }
}
