<!DOCTYPE html>
<html>
<head>
	<title>Create New Acount</title>
</head>
<body>
	<form action="/done" method="POST">
        @csrf
		<h1>Buat Account Baru!</h1>
		<h2>Sign Up Form</h2>
		<label for="fname">First name:</label><br>
		<input type="text" id="fname" name="fname"><br><br>

		<label for="lname">Last name:</label><br>
		<input type="text" id="lname" name="lname"><br><br>

		<label>Gender:</label><br>
		<input type="radio" id="male" name="gender" value="male">
  		<label for="male">Male</label><br>

  		<input type="radio" id="female" name="gender" value="female">
		<label for="female">Female</label><br>

  		<input type="radio" id="other" name="gender" value="other">
  		<label for="other">Other</label><br><br>

		<label>Nationality</label><br>
		<select>
			<option>Indonesia</option>
			<option>Singapura</option>
			<option>Malaysia</option>
			<option>Amerika</option>
		</select><br><br>

		<label>Language Spoken</label><br>
			<input type="checkbox" id="bhs1" name="bhs1" value="Bahasa Indonesia">
 			<label for="bhs1"> Bahasa Indonesia</label><br>

 			<input type="checkbox" id="bhs2" name="bhs2" value="English">
  			<label for="bhs2">English</label><br>

  			<input type="checkbox" id="bhs3" name="bhs3" value="Other">
 			<label for="vehicle3">Other</label>
 			<br><br>

 		<label>Bio:</label><br>
 		<textarea rows="8" cols="35"></textarea><br>

 		<input type="submit" name="submit" >

	</form>

</body>
</html>
